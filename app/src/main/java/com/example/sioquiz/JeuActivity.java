package com.example.sioquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class JeuActivity extends Activity {
    TextView joueur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jeu);

        String message = this.getIntent().getExtras().getString("Joueur");
        joueur = (TextView) this.findViewById(R.id.txt_joueur);
        joueur.setText(joueur.getText()+ " "+ message+" : ");

        // Initialisation des contrôles
        TextView laQuestion = (TextView) findViewById(R.id.txvQuestion); // récup de la TextView pour question
        laQuestion.setMinimumWidth(400);
        RadioGroup groupQuestions = (RadioGroup) findViewById(R.id.rdgQuestion); // récup du RadioGroup
        RadioButton[] reponses = new RadioButton[3]; // initialisation des boutons radio
// Création des boutons radio
        reponses[0] = new RadioButton(this);
        reponses[0].setTextColor(Color.parseColor("#1111FF"));
        reponses[0].setMinimumWidth(400);
        reponses[1] = new RadioButton(this);
        reponses[1].setTextColor(Color.parseColor("#1111FF"));
        reponses[1].setMinimumWidth(400);
        reponses[2] = new RadioButton(this);
        reponses[2].setTextColor(Color.parseColor("#1111FF"));
        reponses[2].setMinimumWidth(400);
// Affectation d'un ID aux boutons radio afin de savoir lequel est sélectionné
        reponses[0].setId(0);
        reponses[1].setId(1);
        reponses[2].setId(2);
// Gestion du bouton
        Button resultat = (Button) findViewById(R.id.btnReponse);
        resultat.setOnClickListener(this);
// Démarrage du jeu
        initQuestions(numero);
        private void initQuestions(int numero) {
            Log.i("numero",""+numero);
            laQuestion.setText("Dans quel film trouve-t-on le personnage de "+questions[numero]
                    [0]+ " ?");
            reponses[0].setText(questions[numero][1]);
            groupQuestions.addView(reponses[0]);
            reponses[1].setText(questions[numero][2]);
            groupQuestions.addView(reponses[1]);
            reponses[2].setText(questions[numero][3]);
            groupQuestions.addView(reponses[2]);
        }
    }
    public void onClick(View v) {
        RadioGroup groupQuestions;
        int retour = groupQuestions.getCheckedRadioButtonId();
        int repOk = Integer.parseInt(questions[numero][4]);
        if (retour+1 == repOk) {
            Toast.makeText(this,"Bonne réponse ! ", Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(this, "Mauvaise réponse, il fallait choisir : "+questions[numero]
                    [repOk], Toast.LENGTH_LONG).show();
        }
// Question suivante
        Intent retourActivPrincipale = new Intent();
        retourActivPrincipale.putExtra("Numero",numero);
        setResult(RESULT_OK, retourActivPrincipale);
        finish();
    }

}