package com.example.sioquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
    Button depart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        depart = (Button) this.findViewById(R.id.bouton_jouer);
        depart.setOnClickListener(this);
        Intent intentJeu = new Intent(this,JeuActivity.class);
        this.startActivityForResult(intentJeu, num );
        // on récupère la valeur saisie dans la zone de saisie
        String leprenom = prenom.getText().toString();
        // on passe à l’intention cette valeur sous le « nom » « Joueur »
        intentJeu.putExtra("Joueur",leprenom);
    }

    @Override
    public void onClick(View v) {
        Intent intentJeu = new Intent(this, JeuActivity.class);
        leprenom = leprenom.getText().toString();
        if(leprenom.equals("")) {
            Toast.makeText(this,"Prenom OBLIGATOIRE !!!!!!!!!!!!!!",Toast.LENGTH_LONG).show(); }
        else {
            lancementJeu(0);

        }
    }

   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode > 1) {


        } else {
            Log i("affichage data intent", "data : "+data.getExtras();getInt("Numero"));
            lancementJeu(requestCode+1);
        }
   }

   public void lancementJeu(int num) {
        intentJeu.putExtra("Joueur" ,leprenom);
        intentJeu.putExtra("Numero" ,num);
        this.startActivityForResult(IntentJeu, num);
   }
}